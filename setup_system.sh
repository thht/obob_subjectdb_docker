#!/usr/bin/env bash

cp repos/docker.repo /etc/yum.repos.d/

yum install -y docker-engine
systemctl enable docker
systemctl start docker

cp systemd/mridb.service /etc/systemd/system/

systemctl enable mridb

cp cron_backup.cron /etc/cron.d/
mkdir /root/backup_db/
systemctl restart crond

./update_subjectdb.sh