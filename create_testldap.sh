#!/usr/bin/env bash

# Copyright (c) 2016, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

BASEDIR=`dirname $(realpath $BASH_SOURCE)`

echo $BASEDIR

NETWORK=$(docker network ls -q -f NAME=obob_subjectdb)
if [ -z "$NETWORK" ]; then
docker network create -d bridge obob_subjectdb
fi

docker create -e LDAP_ORGANISATION=thht -e LDAP_DOMAIN=th-ht.de -e LDAP_ADMIN_PASSWORD=thht-admin -e LDAP_CONFIG_PASSWORD=thht-config --net obob_subjectdb --name testldap thht/openldap