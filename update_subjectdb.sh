#!/usr/bin/env bash

# Copyright (c) 2016, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

BASEDIR=`dirname $(realpath $BASH_SOURCE)`
nopull=${1:-dopull}

if [ $nopull != "-nopull" ]; then
docker pull thht/obob_subjectdb:latest
fi

OLD_CONTAINER=$(docker ps -a -q -f NAME=obob_subjectdb)
TMP_CONTAINER=$(docker ps -a -q -f NAME=obob_subjectdb_new)
NETWORK=$(docker network ls -q -f NAME=obob_subjectdb)
if [ -z "$NETWORK" ]; then
docker network create -d bridge obob_subjectdb
fi

if [ "$TMP_CONTAINER" ]; then
docker rm obob_subjectdb_new
fi

docker create -p 80:80 -p 443:443 --shm-size 512M --net obob_subjectdb -v $BASEDIR/persistent/data:/volumes/persistent_data -v $BASEDIR/persistent/keys:/volumes/keys -v $BASEDIR/persistent/nginxlog:/var/log/nginx --name obob_subjectdb_new thht/obob_subjectdb:latest

if [ "$OLD_CONTAINER" ]; then
docker stop obob_subjectdb
docker rm obob_subjectdb
fi

docker rename obob_subjectdb_new obob_subjectdb
docker start obob_subjectdb